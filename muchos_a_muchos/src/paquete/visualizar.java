/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Lucesita
 */
public class visualizar {
    
        
            public void  visualizar_orden(){
            EntityManagerFactory emf=Persistence.createEntityManagerFactory("muchos_a_muchosPU");
            EntityManager em=emf.createEntityManager();
    
                em.getTransaction().begin();
            
            Collection<Orden> lista_orden=em.createNamedQuery("Orden.findAll").getResultList();
            //para desplegar lista en java
            for(Orden o: lista_orden){
                System.out.println("|-------------------------| ");
                System.out.println("id: "+o.getIdOrden()+" \n");
                System.out.println("fecha "+o.getFechaOrden()+" \n");
                System.out.println("|-------------------------| ");
                System.out.println(" \n");
            }
            
            em.close();
            emf.close();
            }
            
            public void  visualizar_producto(){
            EntityManagerFactory emf=Persistence.createEntityManagerFactory("muchos_a_muchosPU");
            EntityManager em=emf.createEntityManager();
    
                em.getTransaction().begin();
            Collection<Producto> lista_producto=em.createNamedQuery("Producto.findAll").getResultList();
            //para desplegar lista en java
            for(Producto p: lista_producto){
                System.out.println("|-------------------------| ");
                System.out.println("id: "+p.getIdProducto()+" \n");
                System.out.println("descripcion"+p.getDescripcion()+" \n");
                System.out.println("precio "+p.getPrecio()+" \n");
                System.out.println("|-------------------------| ");
            }
            
            em.close();
            emf.close();
            }
     
            public void  visualizar_op1(){
                //utilizando clase Lineaorden.java
            EntityManagerFactory emf=Persistence.createEntityManagerFactory("muchos_a_muchosPU");
            EntityManager em=emf.createEntityManager();
    
                em.getTransaction().begin();
            
            //el metdo para obtener la lista es el: createNameQuery
            Collection<Lineaorden> lista_op1=em.createNamedQuery("Lineaorden.findAll").getResultList();
            //para desplegar lista en java
            
            for(Lineaorden op1: lista_op1){
                
                    System.out.println("|-------------------------|");
                    System.out.println("orden: "+op1.getOrden().getIdOrden()+" \n");
                    System.out.println("fecha: "+op1.getOrden().getFechaOrden()+" \n");
                    System.out.println("producto: "+op1.getProducto().getIdProducto()+" \n");
                    System.out.println("descripcion del producto: "+op1.getProducto().getDescripcion()+" \n");
                    System.out.println("precio del producto: "+op1.getProducto().getPrecio()+" \n");
                    System.out.println("*********");
                    System.out.println("cantidad: "+op1.getCantidad());
                    System.out.println("|-------------------------|");
                    System.out.println(" \n");
                    
            }
            em.close();
            emf.close();
            }
            
            
            /*
            public void  visualizar_op2(){
                //utilizando clase Lineaorden.java
            EntityManagerFactory emf=Persistence.createEntityManagerFactory("muchos_a_muchosPU");
            EntityManager em=emf.createEntityManager();
    
            em.getTransaction().begin();
            
            //el metdo para obtener la lista es el: createNameQuery
            Collection<LineaordenPK> 
                    //=em.createNamedQuery("Lineaorden.findAll").getResultList();
            //para desplegar lista en java
            for(Lineaorden op1: lista_op1){
                System.out.println("cantidad: "+op1.getCantidad());
                System.out.println("orden: "+op1.getOrden());
                System.out.println("orden: "+op1.getProducto());
                
            }
            em.close();
            emf.close();
            }
 */           
    
}
